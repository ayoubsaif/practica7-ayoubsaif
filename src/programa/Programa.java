package programa;

import java.io.IOException;
import java.util.Scanner;

import clases.FicherosAccesoAleatorio;
import clases.FicherosSecuenciales;

public class Programa {
	public static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args) throws IOException {
		int op;
		
		do {
			//llamo al menu principal y devuelvo un int
			op=menuPrincipal();
			switch(op) {
			case 1:
				//Abrir menu Ficheros Secuenciales
				menuFicherosSecuenciales();
				break;
			case 2: 
				//Abrir menu Ficheros Acceso Aleatorio
				menuFicherosAccesoAleatorio();
				break;
			}
		} while  (op<3);
			
	}
	
	public static int menuPrincipal() {
		int opcion = 0;
		boolean err = false;
		do {
			try {
				System.out.println("MENU");
				System.out.println("1.- Abrir m�nu ficheros Secuenciales");
				System.out.println("2.- Abrir menu de Ficheros Acceso Aleatorio");
				System.out.println("3.- Salir");
				System.out.println(" ");
				System.out.println("Elegir opcion:");
				opcion = input.nextInt();
				if (opcion < 1 || opcion > 3) {
					System.out.println("Error, el valor debe estar entre 1 y 3");
					err = true;
				} else {
					err = false;
				}
			} catch (NumberFormatException e) {
				System.out.println("Error, debes introducir un n�mero");
				input.nextLine();
				err = true;
			} catch (Exception e) {
				System.out.println("Error de acceso a la informaci�n del teclado");
				System.out.println("Comprueba tu conexi�n al teclado");
				System.exit(0);
			}
		} while (err);

		return opcion;
	}
		
	public static void menuFicherosSecuenciales() throws IOException {
		int op;
		input.nextLine();
		//1.- pido el nombre del archivo
		System.out.println("Introduce el nombre del Gimnasio a Trabajar");
		String nombreArchivo=input.nextLine();
		nombreArchivo=nombreArchivo+".txt";
		//2.- creare un archivo usando OperacionesConArchivos
		FicherosSecuenciales miArchivo= new FicherosSecuenciales(nombreArchivo);
		
		do {
			//llamo al menu y devuelvo un entero
			op=menuSecuenciales(nombreArchivo);
			switch(op) {
				case 1:
					miArchivo.crearArchivo();
					break;
				case 2: 
					miArchivo.visualizarArchivo();
					break;
				case 3:
					miArchivo.convertirMayuscMinus(1);
					break;
				case 4:
					miArchivo.convertirMayuscMinus(2);
				case 5:
					int cantidadLineas=miArchivo.numeroLineas();
					System.out.println("Numero de lineas "+cantidadLineas);
					break;
				case 6:
					miArchivo.buscarPalabra();
					break;
			}
		} while(0 < op && op < 7);
	}
	
		
	public static int menuSecuenciales(String nombreArchivo) {
		int opcion = 0;
		boolean error = false;
		do {
			try {
				System.out.println("\nMENU GYM: "+nombreArchivo+"\n");
				System.out.println("1. Introduzca nombres de ejercicios: (* cerrar)");
				System.out.println("2. Visualizar ejercicios");
				System.out.println("3. Convertir a may�sculas");
				System.out.println("4. Convertir a min�sculas");
				System.out.println("5. N�mero de lineas");
				System.out.println("6. Buscar palabra en fichero");
				System.out.println("7. Salir");
				System.out.println(" ");
				System.out.println("Elegir opcion");
				opcion = input.nextInt();
				if (opcion < 1 || opcion > 7) {
					System.out.println("Error, el valor debe estar entre 1 y 7");
					error = true;
				} else {
					error = false;
				}

				System.out.println("Opcion :"+opcion);
			} catch (NumberFormatException e) {
				System.out.println("Error, debes introducir un n�mero");
				input.nextLine();
				error = true;
			} catch (Exception e) {
				System.out.println("Error de acceso a la informaci�n del teclado");
				System.out.println("Comprueba tu conexi�n al teclado");
				System.exit(0);
			}

		} while (error);
		return opcion;
	}
	

	public static void menuFicherosAccesoAleatorio() {
		int op;
		input.nextLine();
		FicherosAccesoAleatorio unaLista = new FicherosAccesoAleatorio("ususarios.txt");
		do {
			//llamo al menu y devuelvo un entero
			op=menuAccesoAleatorio();
			switch(op) {
				case 1:
					System.out.println("Rellenar archivo");
					unaLista.rellenarArchivo();
					break;
				case 2: 
					System.out.println("Mostrar archivo");
					unaLista.visualizarArchivo();
					break;
				case 3:
					System.out.println("Modificar archivo");
					unaLista.modificarArchivo();

					System.out.println("Mostrar archivo modificado");
					unaLista.visualizarArchivo();
					break;
				case 4:
					System.out.println("Mostrar archivo modificado");
					unaLista.capitalizarPalabra();
					break;
				case 5:
					System.out.println("Hasta pronto");
					break;
			}
		} while(0 < op && op < 5);
	}
	
	public static int menuAccesoAleatorio() {
		int opcion = 0;
		boolean error = false;
		do {
			try {
				System.out.println("\nMENU FICHEROS ALEATORIOS");
				System.out.println("1. Rellenar el archivo de usuarios:");
				System.out.println("2. Visualizar los usuarios");
				System.out.println("3. Modificar archivo");
				System.out.println("4. Capitalizar letras");
				System.out.println("5. Salir");
				System.out.println(" ");
				System.out.println("Elegir opcion");
				opcion = input.nextInt();
				if (opcion < 1 || opcion > 5) {
					System.out.println("Error, el valor debe estar entre 1 y 7");
					error = true;
				} else {
					error = false;
				}
			} catch (NumberFormatException e) {
				System.out.println("Error, debes introducir un n�mero");
				input.nextLine();
				error = true;
			} catch (Exception e) {
				System.out.println("Error de acceso a la informaci�n del teclado");
				System.out.println("Comprueba tu conexi�n al teclado");
				System.exit(0);
			}
		} while (error);
		return opcion;
	}
	
}
