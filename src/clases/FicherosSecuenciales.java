package clases;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class FicherosSecuenciales {
	public static Scanner input = new Scanner(System.in);
	// campo archivo
	private String archivo;

	// constructor con archivo
	public FicherosSecuenciales(String archivo) {
		this.archivo = archivo;
	}

	public void crearArchivo() throws IOException {
		System.out.println("CREAR ARCHIVO");
		// 1.- abro para lectura y entrada de teclado
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String linea;
		// 2.- abro para escritura con el buffer de lectura
		PrintWriter fuente = new PrintWriter(new BufferedWriter(new FileWriter(this.archivo)));
		System.out.println("Creaci�n de un archivo \n" + "Introducir lineas (* para acabar)");
		// 3.- recorro linea a linea hasta *
		linea = in.readLine();
		while (!linea.equalsIgnoreCase("*")) {
			fuente.println(linea);
			linea = in.readLine();
		}
		System.out.println("archivo creado");
		// 4.- cerrar el archivo
		fuente.close();

	}

	public void visualizarArchivo() {
		String linea;
		System.out.println("Visualiazr los ejercicios del GYM: " + this.archivo);
		try {
			// 1.- abro para lectura
			BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));
			// 2.- recorro linea a linea hasta null
			linea = fuente.readLine();
			while (linea != null) {
				System.out.println(linea);
				linea = fuente.readLine();
			}
			// 3.- cierro el archivo
			fuente.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}
	}

	public void convertirMayuscMinus(int opcionElegida) {
		String linea;
		System.out.println("CONVERTIR MAYUSCULAS/MINUSCULAS");
		if (opcionElegida == 1) {
			System.out.println("May�sculas");
		} else {
			System.out.println("Min�sculas");
		}
		// declaro ArrayList
		ArrayList<String> v = new ArrayList<String>();

		try {
			// 1.- conecto en modo lectura
			BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));
			// 2.- guardo las lineas en un vector
			linea = fuente.readLine();
			while (linea != null) {
				v.add(linea);
				linea = fuente.readLine();
			}
			// 3.- cerrar archivo
			fuente.close();
			// 4.- leo del vector y lo paso al archivo a mayus/minus
			PrintWriter archivo = new PrintWriter(new FileWriter(this.archivo));
			for (int i = 0; i < v.size(); i++) {
				if (opcionElegida == 1) {
					archivo.println(v.get(i).toUpperCase());
				} else {
					archivo.println(v.get(i).toLowerCase());
				}
			}
			// 5.- cierro el archivo
			archivo.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}

	}

	public int numeroLineas() {
		System.out.println("Contar lineas en el archivo:");
		String linea;
		int contadorLineas = 0;

		try {
			// Abrimos el archivo
			BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));
			// 2.- recorro linea a linea hasta null y cuento
			linea = fuente.readLine();
			while (linea != null) {
				contadorLineas++;
				linea = fuente.readLine();
			}
			// 3.- cierro el archivo
			fuente.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}
		return contadorLineas;

	}
 
	/*
	 * M�todo para buscar Palabra y cuantas veces se encuentra
	 * */
	
	public void buscarPalabra() {
		System.out.println("Escriba la palabra a buscar");
		String buscar = input.nextLine();
		try {
			Scanner scan = new Scanner(new File(this.archivo));
			int lineaNum = 0;
			int numPalabras = 0;
	        while(scan.hasNext()){
	        	lineaNum++;
	            String line = scan.nextLine().toLowerCase().toString();
	            if(line.contains(buscar)){
	                System.out.println(line);
	                System.out.println("Encontrado en la linea: "+lineaNum+"\n");
	                numPalabras++;
	            }
	        }
	        if (numPalabras==1) {
	        	System.out.println("Se ha encontrado "+numPalabras+" vez.");
	        }else if(numPalabras>1) {
	        	System.out.println("Se ha encontrado "+numPalabras+" veces.");
	        }else {
	        	System.out.println("No se ha encontrado la palabra: "+buscar);
	        }
            scan.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
			System.exit(0);
		}
	}
}
