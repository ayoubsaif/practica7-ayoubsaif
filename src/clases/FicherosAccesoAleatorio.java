package clases;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;

public class FicherosAccesoAleatorio {
	String archivo;

	public FicherosAccesoAleatorio(String nombreFichero) {
		this.archivo = nombreFichero;
	}

	public void rellenarArchivo() {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			f.seek(f.length());
			String respuesta = "";
			do {
				System.out.println("Nombre ");
				String nombre = in.readLine();
				nombre = formatearNombre(nombre, 20);
				f.writeUTF(nombre);
				System.out.println("�Deseas continuar (si/no)?");
				respuesta = in.readLine();
			} while (respuesta.equalsIgnoreCase("si"));
			f.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}
	}

	private String formatearNombre(String nombre, int lon) {
		// convertir la cadena al tama�o indicado
		if (nombre.length() > lon) {
			// recorta
			return nombre.substring(0, lon);
		} else {
			// completar con espacios en blanco
			for (int i = nombre.length(); i < lon; i++) {
				nombre = nombre + " ";
			}
		}
		return nombre;
	}

	public void visualizarArchivo() {
		try {
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			String nombre = "";
			boolean finFichero = false;
			do {
				try {
					nombre = f.readUTF();
					System.out.println(nombre);
				} catch (EOFException e) {
					System.out.println("Fin fichero");
					finFichero = true;
					f.close();
				}

			} while (!finFichero);
		}catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}
	}

	public void modificarArchivo() {
		try {
			// Pedimos los valores a modificar
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			String oldName = "Nombre";
			String newName = "NombreNuevo";
			System.out.println("Nombre a modificar");
			oldName = in.readLine();
			System.out.println("Dame el nuevo nombre");
			newName = in.readLine();
			// abro el archivo
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			// busco en el archivo y cuando lo encuentre lo modifico
			String nombre;
			boolean finFichero = false;
			boolean valorEncontrado = false;
			do {
				try {
					nombre = f.readUTF();
					if (nombre.trim().equalsIgnoreCase(oldName)) {
						// tengo que modificarlo
						// 20+2 (cadena de 20 + 2 bytes) -> cadena + 2 bytes
						f.seek(f.getFilePointer() - 22);
						newName = formatearNombre(newName, 20);
						f.writeUTF(newName);
						valorEncontrado = true;
					}
				} catch (EOFException e) {
					f.close();
					finFichero = true;
				}
			} while (!finFichero);
			if (valorEncontrado == false) {
				System.out.println("El valor no est� en el fichero");
			} else {
				System.out.println("El valor ha sido modificado");
			}
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}
	}

	public void capitalizarPalabra() {
		try {
			// pido el nombre del valor a buscar
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Nombre a modificar");
			String oldName = in.readLine();
			// abro el archivo
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			// busco en el archivo y cuando lo encuentre lo modifico
			String nombre;
			boolean finFichero = false;
			boolean valorEncontrado = false;
			do {
				try {
					nombre = f.readUTF();
					if (nombre.trim().equalsIgnoreCase(oldName)) {
						// tengo que modificarlo
						// 20+2 (cadena de 20 + 2 bytes) -> cadena + 2 bytes
						f.seek(f.getFilePointer() - 22);
						
				        // get First letter of the string
				        String firstLetStr = nombre.substring(0, 1);
				        // Get remaining letter using substring
				        String remLetStr = nombre.substring(1);
				        // convert the first letter of String to uppercase
				        firstLetStr = firstLetStr.toUpperCase();
				        // concantenate the first letter and remaining string
				        String newNameCapitalized = firstLetStr + remLetStr;
				        
						f.writeUTF(newNameCapitalized);
						valorEncontrado = true;
					}
				} catch (EOFException e) {
					f.close();
					finFichero = true;
				}
			} while (!finFichero);
			if (valorEncontrado == false) {
				System.out.println("El valor no est� en el fichero");
			} else {
				System.out.println("El valor ha sido modificado");
			}
		} catch (FileNotFoundException e) {
			System.out.println("Fichero no encontrado");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error de entrada salida");
			System.exit(0);
		}
    }
}
